/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interceptors;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Logged
@Interceptor
public class LoggedInterceptor implements Serializable {

    private static final long serialVersionUID = -910416423423487519L;

    @AroundInvoke
    public Object meter(InvocationContext context) throws Exception {
        long startTime = 0;
        try {
            System.out.println(context.getTarget());
            startTime = System.currentTimeMillis();
            Logger.getLogger(LoggedInterceptor.class.toString()).log(Level.SEVERE, "->Start method: " + context.getMethod().getName() + " " + startTime + " ms");
            return context.proceed();
        } finally {
            Logger.getLogger(LoggedInterceptor.class.toString()).log(Level.SEVERE, "->End method: " + context.getMethod().getName() + " " + System.currentTimeMillis() + " ms " + "delta: " + (System.currentTimeMillis() - startTime) + "ms");
        }
    }

}
