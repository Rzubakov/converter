
import com.tools.audioconverters.AudioConverter;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;

@SessionScoped
@Named("audioFilesBean")
public class AudioFilesBean implements Serializable {

    @EJB
    private AudioConverter audioConverter;

    private static final long serialVersionUID = 4686706335316339818L;
    private ArrayList<File> files = null;
    private String sessionId = null;
    private String ar = null;
    private String ac = null;
    private String ab = null;
    private Boolean state = false;

    public AudioFilesBean() {
        System.out.println("Construct AudioFilesBean");
        files = new ArrayList<>();
        sessionId = FacesContext.getCurrentInstance().getExternalContext().getSessionId(true);
    }

    public void handleFileUpload(FileUploadEvent event) {
        File file = audioConverter.upload(event.getFile(), sessionId);
        if (!files.contains(file)) {
            files.add(file);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Файл уже присутствует.", file.getName()));
        }
    }

    public void convert(String from, String to) {
        audioConverter.convert(files, ar, ac, ab, sessionId, from, to);
    }

    public void compressFiles() {
        audioConverter.compressFiles(sessionId);
    }

    public StreamedContent getArchive() {
        return audioConverter.getArchive(sessionId);
    }

    public void reset() {
        audioConverter.deleteArchive(sessionId);
        state = false;
        files.clear();
    }

    public ArrayList<File> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<File> files) {
        this.files = files;
    }

    public String getAr() {
        return ar;
    }

    public void setAr(String ar) {
        this.ar = ar;
    }

    public String getAc() {
        return ac;
    }

    public void setAc(String ac) {
        this.ac = ac;
    }

    public String getAb() {
        return ab;
    }

    public void setAb(String ab) {
        this.ab = ab;
    }

    @PostConstruct
    public void init() {
        System.out.println("PostConstruct AudioFilesBean");
        audioConverter.createDir(sessionId);
    }

    @PreDestroy
    public void clear() {
        System.out.println("PreDestroy AudioFilesBean");
        audioConverter.deleteDir(sessionId);
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        System.out.println("getTimeStamp " + Calendar.getInstance().getTime());
        this.state = state;
    }

}
