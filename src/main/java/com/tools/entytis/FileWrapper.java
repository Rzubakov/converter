package com.tools.entytis;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileWrapper {

    private List<File> inputFiles;
    private List<File> convertedFiles;
    private String ac = null;
    private String ab = null;
    private String ar = null;

    public FileWrapper() {
        inputFiles = new ArrayList<>();
        convertedFiles = new ArrayList<>();
    }

    public void addInputFile(File file) {
        inputFiles.add(file);
    }

    public void addConvertedFile(File file) {
        convertedFiles.add(file);
    }

    public List<File> getInputFiles() {
        return inputFiles;
    }

    public void setInputFiles(List<File> inputFiles) {
        this.inputFiles = inputFiles;
    }

    public List<File> getConvertedFiles() {
        return convertedFiles;
    }

    public void setConvertedFiles(List<File> convertedFiles) {
        this.convertedFiles = convertedFiles;
    }

    public String getAc() {
        return ac;
    }

    public void setAc(String ac) {
        this.ac = ac;
    }

    public String getAb() {
        return ab;
    }

    public void setAb(String ab) {
        this.ab = ab;
    }

    public String getAr() {
        return ar;
    }

    public void setAr(String ar) {
        this.ar = ar;
    }

}
