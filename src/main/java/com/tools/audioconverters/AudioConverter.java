package com.tools.audioconverters;

import interceptors.Logged;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.ejb.Stateless;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

@Logged
@Stateless
public class AudioConverter {

    private ProcessBuilder builder;
    private Process process;

    public AudioConverter() {
    }

    public void createDir(String session) {
        try {
            Files.createDirectories(Paths.get("/shareapp/" + session + "/out/"));
            System.out.println("createDir " + "/shareapp/" + session + "/out/");
            Files.createDirectories(Paths.get("/shareapp/" + session + "/in/"));
            System.out.println("createDir " + "/shareapp/" + session + "/in/");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteDir(String session) {
        try {
            builder = new ProcessBuilder("rm", "-rf", "/shareapp/" + session);
            builder.start();
            System.out.println("clear " + "/shareapp/" + session);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File upload(UploadedFile file, String session) {
        System.out.println("upload: " + file.getFileName());
        try {
            Files.copy(file.getInputstream(), Paths.get("/shareapp/" + session + "/in/" + file.getFileName()), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new File("/shareapp/" + session + "/in/" + file.getFileName());
    }

    public void compressFiles(String session) {
        try (FileOutputStream fout = new FileOutputStream("/shareapp/" + session + "/" + session + ".zip"); ZipOutputStream zout = new ZipOutputStream(fout)) {
            File folder = new File("/shareapp/" + session + "/out/");
            for (File file : folder.listFiles()) {
                zout.putNextEntry(new ZipEntry(file.getName()));
                write(new FileInputStream(file), zout);
                Files.delete(file.toPath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public StreamedContent getArchive(String session) {
        try {
            System.out.println("getArchive" + Calendar.getInstance().getTime());
            return new DefaultStreamedContent(new FileInputStream(new File("/shareapp/" + session + "/" + session + ".zip")), "text/plain", session + ".zip");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteArchive(String session) {
        try {
            File file = new File("/shareapp/" + session + "/" + session + ".zip");
            if (Files.exists(file.toPath())) {
                Files.delete(file.toPath());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void write(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int len;
        while ((len = in.read(buffer)) >= 0) {
            out.write(buffer, 0, len);
        }
        in.close();
    }

    public void convert(List<File> files, String ar, String ac, String ab, String session, String from, String to) {

        System.out.println("Size:" + files.size() + " / " + files);
        files.forEach(file -> {
            try {
                File log = new File("/shareapp/log/" + session + "_" + UUID.randomUUID().toString() + file.getName() + ".txt");
                File out = new File("/shareapp/log/" + session + "_" + UUID.randomUUID().toString() + file.getName() + "out.txt");
                File f = new File("/shareapp/" + session + "/in/" + file.getName());

                String source = f.getAbsolutePath();
                String result = "/shareapp/" + session + "/out/" + file.getName().replace("." + from, "") + "." + to;

                switch (to) {
                    case "m4a":
                        builder = new ProcessBuilder("ffmpeg", "-i", source, "-ar", ar, "-ac", ac, "-f", "mp4", "-c:a", "libfdk_aac", "-y", result);
                        System.out.println(builder.command());
                        break;
                    case "m4r":
                        builder = new ProcessBuilder("ffmpeg", "-i", source, "-ar", ar, "-ac", ac, "-f", "mp4", "-c:a", "libfdk_aac", "-y", result);
                        System.out.println(builder.command());
                        break;
                    case "wav":
                        builder = new ProcessBuilder("ffmpeg", "-i", source, "-ar", ar, "-ac", ac, "-y", result);
                        System.out.println(builder.command());
                        break;
                    default:
                        builder = new ProcessBuilder("ffmpeg", "-i", source, "-ar", ar, "-ac", ac, "-ab", ab, "-y", result);
                        System.out.println(builder.command());
                        break;
                }
                builder.redirectError(log);
                builder.redirectOutput(out);
                process = builder.start();
                process.waitFor();
                if (Files.exists(f.toPath())) {
                    Files.delete(f.toPath());
                } else {
                    System.out.println("File not found:" + f.toPath());
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

}
